﻿using System;

namespace library
{
    interface ILybraryObserver
    {
        void UpdateBookAvailable(Book book);
        void UpdateBookDue(Book book);
    }

    class Reader: ILybraryObserver
    {
        private string name;
        public Reader(string name)
        {
            this.name = name;
        }

        public void UpdateBookAvailable(Book book) { Console.WriteLine($"{name}, книга " +
            $"'{book.GetName()}' {book.GetAuthor()} доступна"); }

        public void UpdateBookDue(Book book) { Console.WriteLine($"{name}, книгу " +
            $"'{book.GetName()}' {book.GetAuthor()} необходимо вернуть {book.dueDate.AddDays(14).Date}"); }

    }
}
