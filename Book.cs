﻿using System;
using System.Collections.Generic;

namespace library
{
    interface IBookData
    {
        string GetName();
        string GetAuthor();
    }

    class Book: IObservable, IBookData //наблюдатель
    {
        private string name;
        private string author;
        private string category;
        private string isbn;
        private bool isAvailable = true;

        public DateTime dueDate;

        private List<ILybraryObserver> readers;

        public Book(string name, string author, string category, string isbn = "none")
        {
            this.name = name;
            this.author = author;
            this.category = category;
            this.isbn = isbn;
            readers = new List<ILybraryObserver>();
        }

        public void Request(ILybraryObserver reader) { readers.Add(reader); }

        public void Take(ILybraryObserver reader)
        {
            if (isAvailable == true)
            {
                dueDate = DateTime.Now;
                isAvailable = false;

                reader.UpdateBookDue(this);
            }
            else
                Console.WriteLine("книга пока недоступна");
        }

        public void Return(ILybraryObserver reader)
        {
            isAvailable = true;
            readers.Remove(reader);

            foreach (var observer in readers) { observer.UpdateBookAvailable(this); }
        }

        public string GetName() { return name; }

        public string GetAuthor() { return author; }
 
    }
}
