﻿using System;

namespace library
{
    interface IObservable
    {
        void Request(ILybraryObserver observer);
        void Take(ILybraryObserver observer);
    }
}
