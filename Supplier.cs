﻿using System;

namespace library
{
    // Паттерн адаптер будет полезен при использовании внешней системы, например, поставщик книг, который имеет свой собственный интерфейс
    // Система должна уметь принимать данные от поставщика, который передает данные в другом формате

    class BookData // тип данных внешней системы
    {
        //конвертация в имя 
        public string GetName() { return "название"; }

        //конвертация в имя автора
        public string GetAuthor() { return "автор"; }
    }

    class SupplierBook // класс внешней системы
    {
        private BookData bookData = new BookData();

        public BookData GetBookData(string isbn) { return bookData; }
    }

    class SupplierAdapter : IBookData //адаптер
    {
        private SupplierBook supplierBook;
        private string isbn;

        public SupplierAdapter(SupplierBook book, string isbn)
        {
            supplierBook = book;
            this.isbn = isbn;
        }

        public string GetName()
        {
            BookData bookData = supplierBook.GetBookData(isbn);
            return bookData.GetName();
        }

        public string GetAuthor()
        {
            BookData bookData = supplierBook.GetBookData(isbn);
            return bookData.GetAuthor();
        }
    }
}
