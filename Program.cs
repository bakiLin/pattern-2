﻿using System;

namespace library
{
    class Program
    {
        static void Main(string[] args)
        {
            Reader reader1 = new Reader("Маша");
            Reader reader2 = new Reader("Ильнур");

            Book book_1 = new Book("Божественная комедия", "Данте Алигьери", "художественная литература");
            SupplierBook bookFromSupplier = new SupplierBook();

            SupplierAdapter adapter = new SupplierAdapter(bookFromSupplier, "978-5-699-19540-4");

            Book book_2 = new Book(adapter.GetName(), adapter.GetAuthor(), "публицистика");

            book_1.Request(reader1);

            book_2.Take(reader2);

            book_1.Take(reader1);

            book_1.Request(reader2);

            book_1.Take(reader2);

            book_1.Return(reader1);
        }
    }
}
